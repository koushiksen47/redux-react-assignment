import React, { Component } from "react";

import CounterControl from "../../components/CounterControl/CounterControl";
import CounterOutput from "../../components/CounterOutput/CounterOutput";
import { connect } from "react-redux";
import * as actionCreator from "../../store/actions/index";
class Counter extends Component {
  render() {
    return (
      <div>
        <CounterOutput value={this.props.counter} />
        <CounterControl
          label="Increment"
          clicked={this.props.onIncrementCounter}
        />
        <CounterControl
          label="Decrement"
          clicked={this.props.onDecrementCounter}
        />
        <CounterControl label="Add 5" clicked={this.props.onAddCounter} />
        <CounterControl
          label="Subtract 5"
          clicked={this.props.onSubstarctCounter}
        />
        <hr />
        <button onClick={() => this.props.onStoreResult(this.props.counter)}>
          Store Result
        </button>
        <ul>
          {this.props.results.map(result => {
            return (
              <li
                onClick={() => this.props.onDeleteResult(result.id)}
                key={result.id}
              >
                {result.value}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    counter: state.counter.counter,
    results: state.results.results
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onIncrementCounter: () => {
      dispatch(actionCreator.increment());
    },
    onDecrementCounter: () => {
      dispatch(actionCreator.decrement());
    },
    onAddCounter: () => {
      dispatch(actionCreator.add(5));
    },
    onSubstarctCounter: () => {
      dispatch(actionCreator.substract(15));
    },
    onStoreResult: counterValue => {
      dispatch(actionCreator.saveResult(counterValue));
    },
    onDeleteResult: id => {
      dispatch(actionCreator.deleteResult(id));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
