import * as actionTypes from "./actionTypes";
export const storeResult = res => {
  return {
    type: actionTypes.STORE_RESULT,
    counterValue: res
  };
};
export const deleteResult = id => {
  return {
    type: actionTypes.DELETE_RESULT,
    removeElemId: id
  };
};
export const saveResult = res => {
  return dispatch => {
    setTimeout(() => {
      dispatch(storeResult(res));
    }, 2000);
  };
};
