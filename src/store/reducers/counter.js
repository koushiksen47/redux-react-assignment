import * as actionType from "../actions/actionTypes";
const initialState = {
  counter: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.INCREMENT:
      return { ...state, counter: state.counter + 1 };
      break;
    case actionType.DECREMENT:
      return { ...state, counter: state.counter - 1 };
      break;
    case actionType.ADD:
      return { ...state, counter: state.counter + action.value };
      break;
    case actionType.SUBSTRACT:
      return { ...state, counter: state.counter - action.value };
      break;
  }

  return state;
};

export default reducer;
