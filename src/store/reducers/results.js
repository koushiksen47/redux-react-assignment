import * as actionType from "../actions/actionTypes";
const initialState = {
  results: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.STORE_RESULT:
      return {
        ...state,
        results: state.results.concat({
          id: new Date(),
          value: action.counterValue
        })
      }; // push will mutate the original state
      break;
    case actionType.DELETE_RESULT:
      const updatedArray = state.results.filter(
        result => result.id !== action.removeElemId
      );
      return { ...state, results: updatedArray };
      break;
  }

  return state;
};

export default reducer;
